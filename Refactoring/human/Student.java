package com.javarush.task.task29.task2909.human;

import java.util.Date;

public class Student extends UniversityPerson {
    private double averageGrade;
    private Date beginningOfSession;
    private Date endOfSession;
    private int course;

    public Student(String name, int age, double averageGrade) {
        super(name, age);
        this.averageGrade = averageGrade;
    }

    public void live() {
        learn();
    }

    public void learn() {
    }

    public String getPosition() {
        return "Студент";
    }

    public void incAverageGrade(double delta) {
        setAverageGrade(averageGrade += delta);
        getAverageGrade();
    }

    public void setCourse(int course) {
        this.course = course;
    }

    public void setAverageGrade(double grade) {
        this.averageGrade = grade;
    }

    public void setBeginningOfSession(Date date) {
        beginningOfSession = date;
    }

    public void setEndOfSession(Date date) {
        endOfSession = date;
    }

    public double getAverageGrade() {
        return averageGrade;
    }

    public int getCourse() {
        return course;
    }
}
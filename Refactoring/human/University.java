package com.javarush.task.task29.task2909.human;

import java.rmi.StubNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class University {

    String name;
    int age;
    private List<Student> students = new ArrayList<>();

    public University(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Student getStudentWithAverageGrade(double average) {
        for (Student student : students) {
            if (student.getAverageGrade() == average) {
                return student;
            }
        }
        return null;
    }

    public Student getStudentWithMaxAverageGrade() {

        return students.stream().max(Comparator.comparing(Student::getAverageGrade)).get();
    }

    public Student getStudentWithMinAverageGrade() {
        return students.stream().min(Comparator.comparing(Student::getAverageGrade)).get();
    }

    public void expel(Student student) {
        students.remove(student);

    }
}